﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Linq;

class Password
{
    class UserAccount
    {
        public string Name { get; set; }
        public string PasswordHash { get; set; }
    }

    static string userAccountsFile = "userAccounts.txt";
    static void CreateUserAccount(string name, string password)
    {
        string passwordHash = HashPassword(password);
        UserAccount userAccount = new UserAccount();
        userAccount.Name = name;
        userAccount.PasswordHash = passwordHash;


        using (StreamWriter writer = new StreamWriter(userAccountsFile, true))
        {
            writer.WriteLine($"{userAccount.Name},{userAccount.PasswordHash}");
        }

        Console.WriteLine("User account created successfully!");
    }
    static string HashPassword(string password)
    {
        
        byte[] passwordBytes = Encoding.UTF8.GetBytes(password);


        SHA256 sha256 = SHA256.Create();
        byte[] hashBytes = sha256.ComputeHash(passwordBytes);
        string hashString = BitConverter.ToString(hashBytes).Replace("-", "");

        return hashString;
    }

    static void Main(string[] args)
    {
     
        Console.Write("Enter name: ");
        string name = Console.ReadLine();
        Console.Write("Enter password: ");
        string password = Console.ReadLine();

      
        CreateUserAccount(name, password);
    }
}
