﻿using System;
using System.Collections.Generic;
using System.Linq;

public class Program
{
    public static List<char> GetNonRepeatingChars(string str)
    {
        var nonRepeatingChars = new List<char>();
        var charCount = new Dictionary<char, int>();

        foreach (char c in str)
        {
            if (charCount.ContainsKey(c))
            {
                charCount[c]++;
            }
            else
            {
                charCount.Add(c, 1);
            }
        }

        foreach (KeyValuePair<char, int> pair in charCount)
        {
            if (pair.Value == 1)
            {
                nonRepeatingChars.Add(pair.Key);
            }
        }

        return nonRepeatingChars;
    }

    public static int GetIndexOfFirstNonRepeatingChar(string str)
    {
        var charCount = new Dictionary<char, int>();

        for (int i = 0; i < str.Length; i++)
        {
            char c = str[i];
            if (charCount.ContainsKey(c))
            {
                charCount[c]++;
            }
            else
            {
                charCount.Add(c, 1);
            }
        }

        for (int i = 0; i < str.Length; i++)
        {
            char c = str[i];
            if (charCount[c] == 1)
            {
                return i;
            }
        }

        return -1;
    }

    public static void Main(string[] args)
    {
        string str = "hello world";
        List<char> nonRepeatingChars = GetNonRepeatingChars(str);
        Console.WriteLine("Non-repeating characters in the string:");
        foreach (char c in nonRepeatingChars)
        {
            Console.WriteLine(c);
        }

        int firstNonRepeatingCharIndex = GetIndexOfFirstNonRepeatingChar(str);
        Console.WriteLine("Index of the first non-repeating character in the string: " + firstNonRepeatingCharIndex);
    }
}
